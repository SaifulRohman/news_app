package com.test.news.model

import com.google.gson.annotations.SerializedName

data class HeaderSourceRes (
    @SerializedName("status")
    val status: String? = "",
    @SerializedName("sources")
    val sources: List<SourceRes>? = arrayListOf()
) {
    fun mapToHeaderSourceModel() = HeaderSourceModel(
        status = status.orEmpty(),
        sources = sources?.toListSourceModel() ?: arrayListOf()
    )
}

fun List<SourceRes>.toListSourceModel() : List<SourceModel> {
    return this.map {
        it.mapToSourceModel()
    }
}