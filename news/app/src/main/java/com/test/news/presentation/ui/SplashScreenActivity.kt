package com.test.news.presentation.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.test.news.databinding.ActivitySplashscreenBinding

class SplashScreenActivity : AppCompatActivity() {

    private var isLogin: Boolean = true
    private lateinit var binding: ActivitySplashscreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashscreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Handler(Looper.getMainLooper()).postDelayed({ processLogin() },2000)
    }

    private fun processLogin() {
        if (isLogin) { startActivity(Intent(this, CategoryNewsActivity::class.java)) }
    }
}