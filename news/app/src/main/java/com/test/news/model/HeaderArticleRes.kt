package com.test.news.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HeaderArticleRes (
    @SerializedName("status")
    @Expose
    val status: String? = null,
    @SerializedName("totalResults")
    @Expose
    val totalResults: Int? = 0,
    @SerializedName("articles")
    @Expose
    val articles: List<ArticleRes>? = arrayListOf()
) {
    fun mapToArticleModel() : HeaderArticleModel {
        return HeaderArticleModel(
            status = status.orEmpty(),
            totalResults = totalResults ?: 0,
            article = articles?.mapToArticles() ?: arrayListOf()
        )
    }
}

fun List<ArticleRes>.mapToArticles(): List<ArticleModel> {
    return this.map {
        it.mapToArticleModel()
    }
}

