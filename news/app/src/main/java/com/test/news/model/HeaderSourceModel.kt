package com.test.news.model

data class HeaderSourceModel(
    val status: String = "",
    val sources: List<SourceModel> = arrayListOf()
)
