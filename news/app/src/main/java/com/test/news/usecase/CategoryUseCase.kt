package com.test.news.usecase

import com.test.news.config.CustomApiCallback
import com.test.news.config.GlobalConstant
import com.test.news.model.HeaderArticleRes
import com.test.news.model.HeaderSourceRes
import com.test.news.network.ApiService
import io.reactivex.rxjava3.exceptions.UndeliverableException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.ConnectException
import javax.inject.Inject

class CategoryUseCase @Inject constructor(
    private val apiService: ApiService
) {

    fun getAllSourceNews(
        scope: CoroutineScope,
        apiKey :String,
        callback: CustomApiCallback<HeaderSourceRes>
    ) {
        scope.launch(Dispatchers.IO) {
            try {
                val call: Call<HeaderSourceRes> = apiService.getAllSourceNews(apiKey)
                call.enqueue(object : Callback<HeaderSourceRes>{

                    override fun onResponse(call: Call<HeaderSourceRes>, response: Response<HeaderSourceRes>) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            callback.onSuccess(data)
                        }
                    }

                    override fun onFailure(call: Call<HeaderSourceRes>, t: Throwable) {
                        callback.onError(
                            t.message,
                            GlobalConstant.FAILE_CONNECT_SERVER_CODE
                        )
                    }

                })
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }catch (ex: UndeliverableException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: HttpException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: IOException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: ConnectException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }
        }
    }

    fun getSourceByCategory(
        scope: CoroutineScope,
        category: String,
        apiKey :String,
        callback: CustomApiCallback<HeaderSourceRes>
    ) {
        scope.launch(Dispatchers.IO) {
            try {
                val call: Call<HeaderSourceRes> = apiService.getSourceByCategory(category, apiKey)
                call.enqueue(object : Callback<HeaderSourceRes>{

                    override fun onResponse(call: Call<HeaderSourceRes>, response: Response<HeaderSourceRes>) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            callback.onSuccess(data)
                        }
                    }

                    override fun onFailure(call: Call<HeaderSourceRes>, t: Throwable) {
                        callback.onError(
                            t.message,
                            GlobalConstant.FAILE_CONNECT_SERVER_CODE
                        )
                    }

                })
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }catch (ex: UndeliverableException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: HttpException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: IOException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: ConnectException) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            } catch (ex: Exception) {
                callback.onError("${GlobalConstant.FAIL_CONNECT_SERVER}\n${ex.message}",
                    GlobalConstant.FAILE_CONNECT_SERVER_CODE)
            }
        }
    }

}