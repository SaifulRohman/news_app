package com.test.news.presentation.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View.OnClickListener
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.test.news.R
import com.test.news.config.GlobalConstant
import com.test.news.config.UIState
import com.test.news.databinding.ActivityCategoryBinding
import com.test.news.model.SourceModel
import com.test.news.presentation.adapter.SourceAdapter
import com.test.news.viewmodel.CategoryNewsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategoryNewsActivity : AppCompatActivity() {

    private val viewModel: CategoryNewsViewModel by viewModels()

    private lateinit var binding: ActivityCategoryBinding
    private var categories: List<String> = listOf(
        "business",
        "entertainment",
        "general",
        "health",
        "science",
        "sports",
        "technology"
    )
    private var doubleBackToExitPressedOnce = false

    companion object{
        const val TITLE_BAR_SOURCE = "SOURCE NEWS"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        onObservable()
        setCategories()
        binding.incldCategory.txvTitleHeaderBar.text = TITLE_BAR_SOURCE
        binding.incldCategory.imgVBack.isVisible = false
    }

    private fun onObservable() {
        viewModel.categorieLiveState.observe(this){state ->
            if(state.equals(UIState.ERROR)) {
                hideLoading()
                Toast.makeText(
                    this,
                    viewModel.categorieLiveMessage.value?.second,
                    Toast.LENGTH_LONG
                ).show()
            }
            if (state.equals(UIState.LOADING)) showLoading()
            if (state.equals(UIState.SUCCESS)) hideLoading()
        }

        viewModel.categorieLiveData.observe(this){data ->
            setListAdapter(data)
        }

        viewModel.getAllSourceNews()
    }

    private fun showLoading() {
        binding.prgrsBarCategory.isVisible = true
        binding.rcylrSource.isVisible = false
    }

    private fun hideLoading() {
        binding.prgrsBarCategory.isVisible = false
        binding.rcylrSource.isVisible = true
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setListAdapter (data: List<SourceModel>) {
        val adapter = SourceAdapter(data, onClick = {id ->
            val intent = Intent(this, ArticleActivity::class.java)
            intent.putExtra(GlobalConstant.ID_SOURCE_PARAM, id)
            startActivity(intent)
        })
        binding.rcylrSource.layoutManager = LinearLayoutManager(this)
        binding.rcylrSource.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    private fun setCategories() {
        categories.forEach { tagName ->
            binding.chipGroupCategory.addView(createTagChip(this, tagName))
        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.finish()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            doubleBackToExitPressedOnce = false
        }, 2000)
    }

    private fun createTagChip(context: Context, chipName: String): Chip {
        return Chip(context).apply {
            text = chipName
            setChipBackgroundColorResource(R.color.yellow)
            isCloseIconVisible = true
            setTextColor(ContextCompat.getColor(context, R.color.black))
            setTextAppearance(R.style.ChipTextAppearance)
            setOnClickListener {
                viewModel.getSourceByCategory(chipName)
            }
        }
    }
}