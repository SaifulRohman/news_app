package com.test.news.model

import java.io.Serializable

data class HeaderArticleModel (
    val status: String = "",
    val totalResults: Int = 0,
    val article: List<ArticleModel> = arrayListOf()
): Serializable