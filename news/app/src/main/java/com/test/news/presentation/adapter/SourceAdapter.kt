package com.test.news.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.test.news.databinding.SourceItemBinding
import com.test.news.model.SourceModel

class SourceAdapter(
    val news: List<SourceModel>,
    val onClick: ((String) -> Unit)? = null
): RecyclerView.Adapter<SourceAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            SourceItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount() = news.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(news[position])
    }

    inner class NewsViewHolder(private val binding: SourceItemBinding): ViewHolder(binding.root) {
        fun bind(item: SourceModel) {
            binding.txvName.text = item.name
            binding.txvDescription.text = item.description
            binding.layoutSource.setOnClickListener {
                onClick?.invoke(item.id)
            }
        }
    }

}